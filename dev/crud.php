<?php

class Crud
{

    public $nome;
    public $sku;
    public $categoria;
    public $preco;
    public $descricao;
    public $cod;
    public $qtd;
    public $image;

    public function __construct()
    {

        include 'conn.php';
        $this->conn = $conn;

    }

    public function insertProduct($nome, $sku, $categoria, $preco, $descricao, $qtd, $image)
    {
        $conn = $this->conn;

        //QUERY DE INSERT NO BD
        $sql = "INSERT INTO produto (nome, sku, categoria,preço,descricao,quantidade,imagem)
VALUES ('$nome','$sku','$categoria','$preco','$descricao','$qtd','$image')";

        //VERIFICAÇÃO DE ERRO
        if ($conn->query($sql) === true) {
            echo "Novo Produto Adicionado !";
            header('Location: ../addProduct.php?msg=Produto Adicionado !');

        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $conn->close();

    }

    public function editProduct($nome, $sku, $categoria, $preco, $descricao, $qtd, $image, $id)
    {
        $conn = $this->conn;

        //QUERY DE INSERT NO BD
        if ($image != null) {
            $sql = "UPDATE produto set nome='$nome', sku='$sku', categoria='$categoria',preço='$preco',descricao='$descricao',quantidade='$qtd',imagem='$image' where id='$id'";
        } else {
            $sql = "UPDATE produto set nome='$nome', sku='$sku', categoria='$categoria',preço='$preco',descricao='$descricao',quantidade='$qtd' where id='$id'";

        }
        //VERIFICAÇÃO DE ERRO
        if ($conn->query($sql) === true) {
            echo "Novo Produto Adicionado !";

            echo $sql;
            header('Location: ../products.php?msg=Produto Editado !');

        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }

        $conn->close();

    }

    public function deleteProduct($id)
    {
        $conn = $this->conn;

        //QUERY DE Delete NO BD
        $sql = "DELETE from produto Where id='$id'";

        if ($conn->query($sql) === true) {
            echo " Produto Deletado !";
            header('Location: ../products.php?msg=Produto Deletado !');
            exit();

        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            header('Location: ../products.php?msg=Erro ao deletar');

        }

        $conn->close();

    }

    public function insertCategory($nome, $cod)
    {
        $conn = $this->conn;

        $sql = "INSERT INTO categoria (nome, codigo)
         VALUES ('$nome','$cod')";

        if ($conn->query($sql) === true) {
            echo "Nova Categoria Adicionada !";
            header('Location: ../addCategory.php?msg=Categoria Adicionada !');
            exit();

        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            header('Location: ./addCategory.php?msgErro=Erro ao Adicionar Categoria ');

        }

        $conn->close();

    }

    public function editCategory($nome, $cod, $id)
    {
        $conn = $this->conn;

        //QUERY DE UPDATE NO BD
        $sql = "UPDATE categoria set nome='$nome' , codigo='$cod' Where id='$id'";

        if ($conn->query($sql) === true) {
            echo " Categoria Editada !";
            header('Location: ../categories.php?msg=Categoria Editada !');
            exit();

        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            header('Location: ../categories.php?msg=Erro ao editar');

        }

        $conn->close();

    }

    public function deleteCategory($id)
    {
        $conn = $this->conn;

        //QUERY DE UPDATE NO BD
        $sql = "DELETE from categoria Where id='$id'";

        if ($conn->query($sql) === true) {
            echo " Categoria Deletada !";
            header('Location: ../categories.php?msg=Categoria Deletada !');
            exit();

        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
            header('Location: ../categories.php?msg=Erro ao deletar');

        }

        $conn->close();

    }

}