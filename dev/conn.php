<?php
$servername = "localhost";
$database = "desafio";
$username = "root";
$password = "";

// Conexão com db
$conn = new mysqli($servername, $username, $password, $database);

// Verifica por erros
if ($conn->connect_error) {
    die("Erro ao Conectar Banco de Dados: " . $conn->connect_error);
}
