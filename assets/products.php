<?php include "../dev/conn.php";?>

<!doctype html>
<html ⚡>

<head>
    <title>Webjump | Backend Test | Products</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <link rel="stylesheet" type="text/css" media="all" href="css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">

    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js">
    </script>
    <meta name="viewport" content="width=device-width,minimum-scale=1">
    <style amp-boilerplate>
    body {
        -webkit-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        -moz-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        -ms-animation: -amp-start 8s steps(1, end) 0s 1 normal both;
        animation: -amp-start 8s steps(1, end) 0s 1 normal both
    }

    @-webkit-keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }

    @-moz-keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }

    @-ms-keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }

    @-o-keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }

    @keyframes -amp-start {
        from {
            visibility: hidden
        }

        to {
            visibility: visible
        }
    }
    </style><noscript>
        <style amp-boilerplate>
        body {
            -webkit-animation: none;
            -moz-animation: none;
            -ms-animation: none;
            animation: none
        }
        </style>
    </noscript>
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
</head>
<!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
    <div class="close-menu">
        <a on="tap:sidebar.toggle">
            <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
        </a>
    </div>
    <a href="dashboard.php"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
    <div>
        <ul>
            <li><a href="categories.php" class="link-menu">Categorias</a></li>
            <li><a href="products.php" class="link-menu">Produtos</a></li>
        </ul>
    </div>
</amp-sidebar>
<header>
    <div class="go-menu">
        <a on="tap:sidebar.toggle">☰</a>
        <a href="dashboard.php" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69"
                height="430" /></a>
    </div>
    <div class="right-box">
        <span class="go-title">Administration Panel</span>
    </div>
</header>
<!-- Header -->

<body>
    <!-- Main Content -->
    <main class="content">
        <div class="header-list-page">
            <h1 class="title">Products</h1>
            <a href="addProduct.php" class="btn-action">Add new Product</a>
        </div>


        <?php
//MENSAGEM  DE RETORNO (SUCESSO OU ERRO)
if (isset($_GET['msg'])) {

    $mensagem = $_GET['msg'];

    echo "<div class='alert alert-success'>" . $mensagem . "</div>";

}

?>
        <table class="table" id="table" name="table">
            <thead>
                <tr>

                    <th> Product Image</th>
                    <th> Product Name</th>
                    <th> Product Sku</th>
                    <th> Product Price</th>
                    <th> Product Quantity</th>
                    <th> Product Category</th>
                    <th> Product Description</th>
                    <th> Actions </th>
                </tr>

            </thead>
            <tbody>
                <?php

//QUERY DE SELECT NO BD
$query = "select  *from produto";
$result = $conn->query($query);

if ($result->num_rows > 0) {

    //RETORNA TODOS OS RESULTADOS DO BD
    while ($produto = $result->fetch_assoc()):

    ?>
                <tr>

                    <td><img width="200px" height="200px" src="./produto/uploads/<?php echo $produto['imagem']; ?>">
                    </td>
                    <td><?php echo $produto['nome']; ?></td>
                    <td><?php echo $produto['sku']; ?></td>
                    <td><?php echo $produto['preço']; ?></td>
                    <td><?php echo $produto['quantidade']; ?></td>
                    <td><?php  $decode = json_decode($produto['categoria']);
                    
                    for ($i=0 ; $i<count($decode);$i++) {
                      echo $decode[$i].' - ';
                    }
             
                    
                    ?></td>
                    <td><?php echo $produto['descricao']; ?></td>

                    <td class="">
                        <div class="actions">
                            <div class="action edit"><a href="./editProduct.php?id=<?php echo $produto['id']; ?>"><i
                                        class="fas fa-edit"></i> </a></div>
                            <div class="action delete"> <a
                                    href="./produto/deleteProduto.php?id=<?php echo $produto['id']; ?>"><i
                                        class="fas fa-trash-alt"></i> </a> </div>
                        </div>
                    </td>
                </tr>
                <?php endwhile;}?>
            </tbody>
        </table>
    </main>
    <!-- Main Content -->

    <!-- Footer -->
    <footer>
        <div class="footer-image">
            <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
        </div>
        <div class="email-content">
            <span>go@jumpers.com.br</span>
        </div>
    </footer>
    <!-- Footer -->
</body>

</html>

<script>
$(document).ready(function() {
    $('#table').DataTable();
});
</script>
