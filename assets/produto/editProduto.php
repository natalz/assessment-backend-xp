<?php
require "../../dev/crud.php";

//INSTANCIA DA CLASSE
$editProduto = new Crud;

//VERIFICAÇÃO SE ALGUM DADO FOI PASSADO VIA POST
if (isset($_REQUEST['sku'])):

    if(isset($_FILES['fileToUpload']))
   {
       date_default_timezone_set("Brazil/East"); //Definindo timezone padrão
 
       $ext = strtolower(substr($_FILES['fileToUpload']['name'],-4)); //Pegando extensão do arquivo
       $new_name = date("dmY-His") . $ext; //Definindo um novo nome para o arquivo
       $dir = 'uploads/'; //Diretório para uploads
 
       $move=move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
    
   }

//VARIAVEIS DO FORM
    $skuProduto = $_REQUEST['sku'];
    $id = $_REQUEST['id'];
    $nomeProduto = $_REQUEST['name'];
    $precoProduto = $_REQUEST['price'];
    $quantidadeProduto = $_REQUEST['quantity'];
    $categoriaProduto = $_REQUEST['category'];
    $descricaoProduto = $_REQUEST['description'];


    $categoriaJson = json_encode($categoriaProduto);
//UPDATE De PRODUTO NO BD

//VERIFICA SE FOI FEITO UPLOAD DE IMAGEM , SE FOI ATUALIZA A IMAGEM , SE NÃO MANTEM A MESMA IMAGEM.
     if ($move ==TRUE):
    $editProduto->editProduct($nomeProduto, $skuProduto, $categoriaJson,$precoProduto,$descricaoProduto,$quantidadeProduto,$new_name,$id);
     else:
        $editProduto->editProduct($nomeProduto, $skuProduto, $categoriaJson,$precoProduto,$descricaoProduto,$quantidadeProduto,NULL,$id);
     endif;
    
 




endif;
