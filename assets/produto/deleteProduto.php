<?php
require "../../dev/crud.php";

//INSTANCIA DA CLASSE
$deletarProduto= new Crud;

//VERIFICAÇÃO SE ALGUM DADO FOI PASSADO VIA POST
if (isset($_GET['id'])):

//VARIAVEIS DO FORM

    $id = $_GET['id'];

//DELETE Produto  NO BD
    $deletarProduto->deleteProduct($id);

endif;
