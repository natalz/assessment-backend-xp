<?php
require "../../dev/crud.php";

//INSTANCIA DA CLASSE
$saveProduto = new Crud;

//VERIFICAÇÃO SE ALGUM DADO FOI PASSADO VIA POST
if (isset($_REQUEST['sku'])):

   
       date_default_timezone_set("Brazil/East"); //Definindo timezone padrão
 
       $ext = strtolower(substr($_FILES['fileToUpload']['name'],-4)); //Pegando extensão do arquivo
       $new_name = date("dmY-His") . $ext; //Definindo um novo nome para o arquivo
       $dir = 'uploads/'; //Diretório para uploads
 
       move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $dir.$new_name); //Fazer upload do arquivo
    


//VARIAVEIS DO FORM
    $skuProduto = $_REQUEST['sku'];
    $nomeProduto = $_REQUEST['name'];
    $precoProduto = $_REQUEST['price'];
    $quantidadeProduto = $_REQUEST['quantity'];
    $categoriaProduto = $_REQUEST['category'];
    $descricaoProduto = $_REQUEST['description'];


    $categoriaJson = json_encode($categoriaProduto);
//UPDATE DA CATEGORIA NO BD
    $saveProduto->insertProduct($nomeProduto, $skuProduto, $categoriaJson,$precoProduto,$descricaoProduto,$quantidadeProduto,$new_name);
    
    
 




endif;
