<?php
require "../../dev/crud.php";

//INSTANCIA DA CLASSE
$editarCategoria = new Crud;

//VERIFICAÇÃO SE ALGUM DADO FOI PASSADO VIA POST
if (isset($_REQUEST['categoria'])):

//VARIAVEIS DO FORM
    $nomeCategoria = $_REQUEST['categoria'];
    $codigoCategoria = $_REQUEST['codigo'];
    $idCategoria = $_REQUEST['id'];

//UPDATE DA CATEGORIA NO BD
    $editarCategoria->editCategory($nomeCategoria, $codigoCategoria, $idCategoria);

endif;
