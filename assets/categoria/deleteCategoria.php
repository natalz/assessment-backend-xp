<?php
require "../../dev/crud.php";

//INSTANCIA DA CLASSE
$deletarCategoria = new Crud;

//VERIFICAÇÃO SE ALGUM DADO FOI PASSADO VIA POST
if (isset($_GET['id'])):

//VARIAVEIS DO FORM

    $idCategoria = $_GET['id'];

//DELETE DA CATEGORIA NO BD
    $deletarCategoria->deleteCategory($idCategoria);

endif;
