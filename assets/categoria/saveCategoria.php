<?php
require "../../dev/crud.php";

//INSTANCIA DA CLASSE
$inserirCategoria = new Crud;

//VERIFICAÇÃO SE ALGUM DADO FOI PASSADO VIA POST
if (isset($_REQUEST['categoria'])):

//VARIAVEIS DO FORM
    $nomeCategoria = $_REQUEST['categoria'];
    $codigoCategoria = $_REQUEST['codigo'];

//INSERÇÃO DA CATEGORIA NO BD
    $inserirCategoria->insertCategory($nomeCategoria, $codigoCategoria);

endif;
