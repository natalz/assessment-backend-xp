# FERRAMENTAS UTILIZADAS 
- Php 8.0
- Xampp v3.3.0
- Datatables.
- Mysql

# INSTALAÇÃO 
- Para rodar o CRUD utilizei  o Xampp , basta colocar os arquivos dentro da pasta htdocs e rodar o apache e o mysql.
- Entre no phpmyadmin e crie um banco de dados chamado "desafio".
- Importar o arquivo desafio.sql dentro desse banco.
- O Usuário padrão é root e sem senha , caso criem um novo usuario devem mudar no arquivo /dev/conn.php.
- Acessar http://localhost/<nomedasuapastanohtdocs>/assets/dashboard.php e ja estará funcionando.

